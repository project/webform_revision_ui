<?php

declare(strict_types = 1);

namespace Drupal\webform_revision_ui\Controller;

use Drupal\config_revision\Entity\ConfigRevision;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Entity\Controller\VersionHistoryController;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;

/**
 * Revision controller class.
 */
class RevisionController extends ControllerBase {

  /**
   * The controller resolver.
   *
   * @var \Drupal\Core\Controller\ControllerResolverInterface
   */
  protected $controllerResolver;

  /**
   * A router implementation which does not check access.
   *
   * @var \Symfony\Component\Routing\Matcher\RequestMatcherInterface
   */
  protected $accessUnawareRouter;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * RevisionController constructor.
   *
   * @param \Drupal\Core\Controller\ControllerResolverInterface $controller_resolver
   *   The controller resolver.
   * @param \Symfony\Component\Routing\Matcher\RequestMatcherInterface $access_unaware_router
   *   A router implementation which does not check access.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(ControllerResolverInterface $controller_resolver, RequestMatcherInterface $access_unaware_router, RequestStack $request_stack, RouteMatchInterface $route_match, RendererInterface $renderer) {
    $this->controllerResolver = $controller_resolver;
    $this->accessUnawareRouter = $access_unaware_router;
    $this->requestStack = $request_stack;
    $this->routeMatch = $route_match;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('controller_resolver'),
      $container->get('router.no_access_checks'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('renderer')
    );
  }

  /**
   * The revision overview page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request of the page.
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform.
   *
   * @return array
   *   The render array.
   */
  public function revisionOverview(Request $request, WebformInterface $webform) {
    // The corresponding config revision entity.
    $config_revision = ConfigRevision::loadConfigRevisionByConfigId($webform->id());
    // The cacheability metadata.
    $cacheability_metadata = new CacheableMetadata();
    $cacheability_metadata->addCacheableDependency($webform);
    $cacheability_metadata->addCacheableDependency($config_revision);

    // Render config revision version-history page here.
    $controller = $this->controllerResolver->getControllerFromDefinition(VersionHistoryController::class . '::versionHistory');
    // Create a sub-request.
    $url = $config_revision->toUrl('version-history');
    $generated_url = $url->toString(TRUE);
    $cacheability_metadata->addCacheableDependency($generated_url);
    $sub_request = Request::create($generated_url->getGeneratedUrl(), 'GET', $request->query->all(), $request->cookies->all(), [], $request->server->all());
    // Set current attributes of sub-request according to version-history page
    // route parameters.
    $sub_request->attributes->add($this->accessUnawareRouter->matchRequest($sub_request));
    // Carry over the session to the sub-request.
    if ($request->hasSession()) {
      $sub_request->setSession($request->getSession());
    }
    $this->requestStack->push($sub_request);
    $context = new RenderContext();
    $arguments = [$this->routeMatch];
    $build = $this->renderer->executeInRenderContext($context, function () use ($controller, $arguments) {
      // Now call the actual controller, just like HttpKernel does.
      return call_user_func_array($controller, $arguments);
    });
    $this->requestStack->pop();
    if (is_array($build)) {
      $cacheability_metadata->applyTo($build);
    }

    return $build;
  }

}
