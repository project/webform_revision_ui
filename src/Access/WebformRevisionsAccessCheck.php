<?php

declare(strict_types = 1);

namespace Drupal\webform_revision_ui\Access;

use Drupal\config_revision\Entity\ConfigRevision;
use Drupal\config_revision\Entity\ConfigRevisionType;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\webform\WebformInterface;

/**
 * Webform revisions access check class.
 */
class WebformRevisionsAccessCheck implements AccessInterface {

  /**
   * The access method.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   The access result.
   */
  public function access(WebformInterface $webform) {

    // If webform is not revisionable.
    if (!$config_revision_type = ConfigRevisionType::load($webform->getEntityTypeId())) {
      return AccessResult::forbidden('Webform are not revisionable.')
        ->addCacheableDependency($webform)
        ->addCacheableDependency($config_revision_type);
    }

    // If there is no revision saved yet.
    if (!$config_revision = ConfigRevision::loadConfigRevisionByConfigId($webform->id())) {
      return AccessResult::forbidden('Re-save the webform to access the revision tab.')
        ->addCacheableDependency($webform)
        ->addCacheableDependency($config_revision_type)
        ->addCacheableDependency($config_revision);
    }
    return AccessResult::allowed()
      ->addCacheableDependency($webform)
      ->addCacheableDependency($config_revision_type)
      ->addCacheableDependency($config_revision);
  }

}
